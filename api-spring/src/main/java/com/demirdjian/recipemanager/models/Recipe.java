package com.demirdjian.recipemanager.models;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

@Document(collection = "recipes")
public class Recipe {

	@Id
	private String id;

	@TextIndexed
	private String title;
	@TextIndexed
	private String description;
	@TextIndexed
	private Ingredient[] ingredients;
	@TextIndexed
	private String author;

	private String[] steps;
	private Category category;
	private CookingMethod cookingMethod;
	private Integer servings;
	private ArrayList<String> images;

	@CreatedDate
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date creationDate;

	/**
	 * Empty Constructor.
	 */
	public Recipe() {
		creationDate = Date.from(Instant.now());
	}

	/**
	 * Constructor.
	 * 
	 * @param id
	 * @param title
	 * @param ingredients
	 * @param description
	 * @param steps
	 * @param category
	 * @param cookingMethod
	 * @param author
	 * @param servings
	 * @param images
	 */
	public Recipe(String id, String title, Ingredient[] ingredients, String description, String[] steps,
			Category category, CookingMethod cookingMethod, String author, Integer servings, List<String> images) {
		this.id = id;
		this.title = title;
		this.ingredients = ingredients;
		this.description = description;
		this.steps = steps;
		this.category = category;
		this.cookingMethod = cookingMethod;
		this.author = author;
		this.servings = servings;
		this.images = (ArrayList<String>) images;
		this.creationDate = Date.from(Instant.now());
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Ingredient[] getIngredients() {
		return ingredients;
	}

	public void setIngredients(Ingredient[] ingredients) {
		this.ingredients = ingredients;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String[] getSteps() {
		return steps;
	}

	public void setSteps(String[] steps) {
		this.steps = steps;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public CookingMethod getCookingMethod() {
		return cookingMethod;
	}

	public void setCookingMethod(CookingMethod cookingMethod) {
		this.cookingMethod = cookingMethod;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Integer getServings() {
		return servings;
	}

	public void setServings(Integer servings) {
		this.servings = servings;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = (ArrayList<String>) images;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Custom toString for debug printing.
	 * 
	 * @return String
	 */
	public String toString() {
		StringBuilder returnStr = new StringBuilder();
		returnStr.append("ID:\t\t" + this.id + "\n");
		returnStr.append("Title:\t\t" + this.title + "\n");
		returnStr.append("Author:\t\t" + this.author + "\n");
		returnStr.append("Description:\t" + this.description + "\n");
		returnStr.append("Ingredients:\t" + Arrays.toString(this.ingredients) + "\n");
		returnStr.append("Steps:\t\t" + Arrays.toString(this.steps) + "\n");
		returnStr.append("Category:\t" + this.category + "\n");
		returnStr.append("Cooking Method:\t" + this.cookingMethod + "\n");
		returnStr.append("Servings:\t" + this.servings + "\n");
		returnStr.append("Images:\t" + this.images.toString() + "\n");
		returnStr.append("Date Created:\t" + this.creationDate.toString() + "\n");

		return returnStr.toString();
	}

}
