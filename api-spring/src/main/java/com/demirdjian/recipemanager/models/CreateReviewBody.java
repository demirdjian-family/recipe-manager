package com.demirdjian.recipemanager.models;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

public class CreateReviewBody {

	@NotBlank(message = "Author may not be empty.")
	private String author;

	@NotNull(message = "Score may not be empty.")
	@Min(value = 1, message = "Score may not be less than 1.")
	@Max(value = Review.MAX_SCORE, message = "Score may not be greater than " + Review.MAX_SCORE + ".")
	private Integer score;

	@NotBlank(message = "Comment may not be empty.")
	private String comment;

	@NotNull(message = "Date may not be empty.")
	@PastOrPresent(message = "Date must be valid.")
	private Date date;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Custom toString for debug printing.
	 * 
	 * @return String
	 */
	public String toString() {
		StringBuilder returnStr = new StringBuilder();
		returnStr.append("Author:\t\t" + this.author + "\n");
		returnStr.append("Score:\t" + this.score + "\n");
		returnStr.append("Comment:\t" + this.comment + "\n");
		returnStr.append("Date:\t" + this.date.toString() + "\n");

		return returnStr.toString();
	}
}
