package com.demirdjian.recipemanager.models;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "reviews")
public class Review {

	public static final int MAX_SCORE = 5;

	@Id
	private String id;

	@Indexed
	private String recipeId;

	private String author;
	private Integer score;
	private String comment;
	private Date date;

	/**
	 * Empty Constructor.
	 */
	public Review() {
	}

	/**
	 * Constructor.
	 * 
	 * @param id
	 * @param recipeID
	 * @param author
	 * @param score
	 * @param comment
	 * @param date
	 */
	public Review(String id, String recipeID, String author, Integer score, String comment, Date date) {
		this.recipeId = recipeID;
		this.id = id;
		this.author = author;
		this.score = score;
		this.comment = comment;
		this.date = date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(String recipeId) {
		this.recipeId = recipeId;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Custom toString for debug printing.
	 * 
	 * @return String
	 */
	public String toString() {
		StringBuilder returnStr = new StringBuilder();
		returnStr.append("ID:\t\t" + this.id + "\n");
		returnStr.append("Recipe ID:\t\t" + this.recipeId + "\n");
		returnStr.append("Author:\t\t" + this.author + "\n");
		returnStr.append("Score:\t" + this.score + "\n");
		returnStr.append("Comment:\t" + this.comment + "\n");
		returnStr.append("Date:\t" + this.date.toString() + "\n");

		return returnStr.toString();
	}

}
