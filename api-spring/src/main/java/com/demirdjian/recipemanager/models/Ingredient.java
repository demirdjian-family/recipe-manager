package com.demirdjian.recipemanager.models;

public class Ingredient {
	private String name;
	private int quantity;
	private String unit;
	private String flatName;

	/**
	 * Empty Constructor.
	 */
	public Ingredient() {
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 * @param quantity
	 * @param unit
	 * @param flatName
	 */
	public Ingredient(String name, int quantity, String unit, String flatName) {
		this.name = name;
		this.quantity = quantity;
		this.unit = unit;
		this.flatName = flatName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getFlatName() {
		return flatName;
	}

	public void setFlatName(String flatName) {
		this.flatName = flatName;
	}

	/**
	 * Custom toString for debug printing.
	 * 
	 * @return String
	 */
	public String toString() {
		StringBuilder returnStr = new StringBuilder();
		returnStr.append("{Ingredient Name:" + this.name + ",");
		returnStr.append("Quantity:" + this.quantity + ",");
		returnStr.append("Unit:" + this.unit + ",");
		returnStr.append("Flat Name:" + this.flatName + "}");

		return returnStr.toString();
	}

}
