package com.demirdjian.recipemanager.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.demirdjian.recipemanager.models.UpdateReviewBody;

public class UpdateReviewValidator implements ConstraintValidator<UpdateReviewConstraint, UpdateReviewBody> {

	@Override
	public void initialize(UpdateReviewConstraint updateReview) {
		// initializes validator before running isValid
	}

	@Override
	public boolean isValid(UpdateReviewBody updateReview, ConstraintValidatorContext context) {
		// updateReview is invalid if all properties are null

		return !(updateReview.getAuthor() == null && updateReview.getComment() == null
				&& updateReview.getDate() == null && updateReview.getScore() == null);

	}

}
