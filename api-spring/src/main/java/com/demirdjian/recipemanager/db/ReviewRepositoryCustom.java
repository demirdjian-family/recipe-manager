package com.demirdjian.recipemanager.db;

import java.util.List;

import com.demirdjian.recipemanager.models.Review;

public interface ReviewRepositoryCustom {

    /**
     * Find all Reviews with the provided Recipe ID.
     * 
     * @param recipeId
     * @return List<Review>
     */
    public List<Review> findReviews(String recipeId);
}
