package com.demirdjian.recipemanager.db;

import java.util.Date;
import java.util.List;

import com.demirdjian.recipemanager.models.Category;
import com.demirdjian.recipemanager.models.CookingMethod;
import com.demirdjian.recipemanager.models.Recipe;

import org.springframework.data.domain.Sort.Direction;

public interface RecipeRepositoryCustom {

    /**
     * Perform a Full Text Search on the Title and Description. Optionally search by
     * Category and Cooking Method as well.
     * 
     * @param queryText
     * @param category
     * @param cookingMethod
     * @param createdBefore
     * @param createdAfter
     * @param limit
     * @param sortBy
     * @param order
     * @return List<Recipe>
     */
    public List<Recipe> search(String queryText, Category category, CookingMethod cookingMethod, Date createdBefore,
            Date createdAfter, Integer limit, String sortBy, Direction order);
}
