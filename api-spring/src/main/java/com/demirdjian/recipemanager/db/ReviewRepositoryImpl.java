package com.demirdjian.recipemanager.db;

import java.util.List;

import com.demirdjian.recipemanager.models.Review;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class ReviewRepositoryImpl implements ReviewRepositoryCustom {

    @Autowired
    private MongoTemplate template;

    @Override
    public List<Review> findReviews(String recipeId) {
        Query query = new Query();

        query.addCriteria(Criteria.where("recipeId").is(recipeId));

        return template.find(query, Review.class);
    }

}
