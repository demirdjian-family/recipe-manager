package com.demirdjian.recipemanager.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.demirdjian.recipemanager.db.RecipeRepository;
import com.demirdjian.recipemanager.db.ReviewRepository;
import com.demirdjian.recipemanager.models.CreateReviewBody;
import com.demirdjian.recipemanager.models.Recipe;
import com.demirdjian.recipemanager.models.Review;
import com.demirdjian.recipemanager.models.UpdateReviewBody;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/api")
public class ReviewController {

	@Autowired
	private ReviewRepository reviewRepository;
	@Autowired
	private RecipeRepository recipeRepository;
	private static final Logger RM_LOGGER = LoggerFactory.getLogger(ReviewController.class);

	/**
	 * Get all reviews belonging to the given Recipe ID.
	 * 
	 * @param recipeId
	 * @param httpResponse
	 * @return List<Review>
	 */
	@GetMapping("/recipes/{recipeId}/reviews")
	public List<Review> getReviews(@PathVariable("recipeId") @NotBlank String recipeId,
			HttpServletResponse httpResponse) {
		List<Review> response = reviewRepository.findReviews(recipeId);
		if (response.isEmpty()) {
			httpResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
			RM_LOGGER.debug("No Reviews Found.");
		} else {
			RM_LOGGER.debug("Reviews Found: \n{}", response);
		}
		return response;
	}

	/**
	 * Returns the review associated with the provided ID.
	 * 
	 * @param reviewId
	 * @param httpResponse
	 * @return Review
	 */
	@GetMapping("/recipes/{recipeId}/reviews/{reviewId}")
	public Review getReview(@PathVariable("reviewId") @NotBlank String reviewId, HttpServletResponse httpResponse) {
		Optional<Review> response = reviewRepository.findById(reviewId);

		if (response.isPresent()) {
			RM_LOGGER.debug("Review Found: \n{}", response.get());
			return response.get();
		} else {
			httpResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
			RM_LOGGER.debug("No Review Found.");
			return null;
		}
	}

	/**
	 * Adds Review to the Recipe with the provided ID.
	 * 
	 * @param recipeId
	 * @param newReview
	 * @param httpResponse
	 */
	@PostMapping("/recipes/{recipeId}/reviews")
	public void createReview(@PathVariable("recipeId") @NotBlank String recipeId, @Valid @RequestBody CreateReviewBody newReview,
			HttpServletResponse httpResponse) {
		// Check if recipe with recipeId exists
		Optional<Recipe> existingRecipe = recipeRepository.findById(recipeId);
		if (!existingRecipe.isPresent()) {
			httpResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
			RM_LOGGER.debug("No Recipe Found.");
			return;
		}

		Review review = new Review();
		review.setAuthor(newReview.getAuthor());
		review.setComment(newReview.getComment());
		review.setScore(newReview.getScore());
		review.setDate(newReview.getDate());
		review.setRecipeId(recipeId);

		reviewRepository.save(review);

		RM_LOGGER.debug("Review added to Recipe: \n{}, id: {}", review, recipeId);
	}

	/**
	 * Updates the review of the provided ID with the provided values.
	 * 
	 * @param reviewId
	 * @param newReviewParts
	 * @param httpResponse
	 */
	@PatchMapping("/recipes/{recipeId}/reviews/{reviewId}")
	public void updateReview(@PathVariable("reviewId") @NotBlank String reviewId, @Valid @RequestBody UpdateReviewBody newReviewParts,
			HttpServletResponse httpResponse) {
		// Check if review with reviewId exists
		Optional<Review> existingReview = reviewRepository.findById(reviewId);
		if (!existingReview.isPresent()) {
			httpResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
			RM_LOGGER.debug("No Review Found.");
			return;
		}

		// Update newReview with included parts from newReviewParts
		Review newReview = existingReview.get();
		if (newReviewParts.getAuthor() != null && !newReviewParts.getAuthor().isEmpty()) {
			newReview.setAuthor(newReviewParts.getAuthor());
		}
		if (newReviewParts.getComment() != null && !newReviewParts.getComment().isEmpty()) {
			newReview.setComment(newReviewParts.getComment());
		}
		if (newReviewParts.getScore() != null) {
			newReview.setScore(newReviewParts.getScore());
		}
		if (newReviewParts.getDate() != null) {
			newReview.setDate(newReviewParts.getDate());
		}

		// Update the review
		reviewRepository.save(newReview);
		RM_LOGGER.debug("Review Updated: \n{}", newReview);

	}

	/**
	 * Replaces the review of the provided ID with the provided review.
	 * 
	 * @param reviewId
	 * @param newReview
	 * @param httpResponse
	 */
	@PutMapping("/recipes/{recipeId}/reviews/{reviewId}")
	public void replaceReview(@PathVariable("reviewId") @NotBlank String reviewId, @Valid @RequestBody CreateReviewBody newReview,
			HttpServletResponse httpResponse) {
		// Check if review with reviewId exists
		Optional<Review> existingReview = reviewRepository.findById(reviewId);
		if (!existingReview.isPresent()) {
			httpResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
			RM_LOGGER.debug("No Review Found.");
			return;
		}

		Review review = new Review();
		review.setId(reviewId);
		review.setRecipeId(existingReview.get().getRecipeId());
		review.setAuthor(newReview.getAuthor());
		review.setComment(newReview.getComment());
		review.setScore(newReview.getScore());
		review.setDate(newReview.getDate());

		reviewRepository.save(review);
		RM_LOGGER.debug("Review Replaced: \n{}", newReview);
	}

	/**
	 * Deletes the review associated with the provided ID.
	 * 
	 * @param reviewId
	 * @param httpResponse
	 */
	@DeleteMapping("/recipes/{recipeId}/reviews/{reviewId}")
	public void deleteReview(@PathVariable("reviewId") @NotBlank String reviewId, HttpServletResponse httpResponse) {
		Optional<Review> review = reviewRepository.findById(reviewId);
		if (review.isPresent()) {
			reviewRepository.deleteById(reviewId);
			RM_LOGGER.debug("Review Deleted: \n{}", review.get());
		} else {
			httpResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
	}

}
