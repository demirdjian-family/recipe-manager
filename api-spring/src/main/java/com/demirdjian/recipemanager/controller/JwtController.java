package com.demirdjian.recipemanager.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demirdjian.recipemanager.auth.JwtTokenUtil;
import com.demirdjian.recipemanager.auth.JwtUserDetailsService;
import com.demirdjian.recipemanager.models.JwtRequest;
import com.demirdjian.recipemanager.models.JwtResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class JwtController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	private static final Logger RM_LOGGER = LoggerFactory.getLogger(JwtController.class);

	/**
	 * Authenticate a user's session.
	 * 
	 * @param authenticationRequest
	 * @param httpResponse
	 * @return JwtResponse
	 */
	@PostMapping("/authenticate")
	public JwtResponse createAuthenticationToken(@RequestBody JwtRequest authenticationRequest,
			HttpServletResponse httpResponse) {

		if (authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword())) {
			UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
			String token = jwtTokenUtil.generateToken(userDetails);
			return new JwtResponse(token);
		}
		httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		return null;
	}

	/**
	 * Check if user's session token is valid. Returns true if token is valid.
	 * 
	 * @param httpRequest
	 * @param httpResponse
	 * @return boolean
	 */
	@GetMapping("/authenticate")
	public boolean checkAuthenticationToken(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {

		String requestTokenHeader = httpRequest.getHeader("Authorization");
		String jwtToken = null;

		// JWT to be in form "Bearer token". Remove Bearer word.
		if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
			jwtToken = requestTokenHeader.substring("Bearer ".length());

			if (Boolean.FALSE.equals(jwtTokenUtil.isTokenExpired(jwtToken))) {
				httpResponse.setStatus(HttpServletResponse.SC_OK);
				return true;
			}
		}

		httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		return false;
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @return boolean
	 */
	private boolean authenticate(String username, String password) {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			return true;
		} catch (DisabledException e) {
			RM_LOGGER.error("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			RM_LOGGER.error("INVALID_CREDENTIALS", e);
		}
		return false;
	}
}
