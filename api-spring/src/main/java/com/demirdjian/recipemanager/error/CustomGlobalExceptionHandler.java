package com.demirdjian.recipemanager.error;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {
    /**
     * Handle errors for @Valid.
     * 
     * @param ex
     * @param request
     * @return ResponseEntity
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        // Get all errors
        List<String> errors = ex.getBindingResult().getAllErrors().stream().map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());

        return createCustomErrorResponseBody(errors, HttpStatus.BAD_REQUEST);
    }

    /**
     * Defines the handler for the Failed Constraint exceptions.
     * 
     * @param ex
     * @param request
     * @return ResponseEntity
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> constraintViolationException(ConstraintViolationException ex, WebRequest request) {
        // Get all errors
        List<String> errors = ex.getConstraintViolations().stream().map(x -> x.getMessage())
                .collect(Collectors.toList());

        return createCustomErrorResponseBody(errors, HttpStatus.BAD_REQUEST);
    }

    /**
     * Creates a HTTP response body based on the given errors.
     * 
     * @param errors
     * @param status
     * @return ResponseEntity<Object>
     */
    public static ResponseEntity<Object> createCustomErrorResponseBody(List<String> errors, HttpStatus status) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status);
        body.put("errors", errors);

        return new ResponseEntity<>(body, status);
    }
}
