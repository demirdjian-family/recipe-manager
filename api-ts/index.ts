import express from 'express';
import cors from 'cors';

const corsMiddleware = require('cors');
const bodyParser = require('body-parser');

const app: express.Application = express();
const port: number = 8080;

// Use Body Parser
app.use(bodyParser.json());

// Setup CORS
const allowedOrigins = process.env.ALLOWED_ORIGINS?.split(',') || [];
const corsOptions: cors.CorsOptions = {
  origin: (origin: string | undefined, callback: Function) => {
    if (!origin || allowedOrigins.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error(`CORS Error: ${origin} not in Allowed Origins`), false);
    }
  },
};
app.use(corsMiddleware(corsOptions));
app.options(corsMiddleware(corsOptions));

// Auth Endpoints
app.use('/api/auth', require('./routes/AuthRoutes'));

// JWT Middleware
app.use('/api', require('./middleware/JwtMiddleware'));

// Option Endpoints
app.use('/api/options', require('./routes/OptionRoutes'));

// Recipe Endpoints
app.use('/api/recipes', require('./routes/RecipeRoutes'));

// Review Endpoints
app.use('/api/recipes', require('./routes/ReviewRoutes'));

// Image Endpoints
app.use('/api/recipes', require('./routes/ImageRoutes'));

app.listen(port, () => {
  console.log(`Recipe Manger API running on port ${port}.`);
  console.log(`With Allowed Origins: [${allowedOrigins}].`);
});
