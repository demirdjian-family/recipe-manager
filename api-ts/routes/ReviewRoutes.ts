import { Request, Response } from 'express';

import { IReview, Review } from '../models/Review';

const mongoose = require('mongoose');

const express = require('express');

const router = express.Router();

// Get all Reviews for a Recipe
router.get('/:recipeId/reviews', (req: Request, res: Response) => {
  const { recipeId } = req.params;
  const promise = Review.find({ recipeId }).exec();
  promise.then((docs: any) => {
    res.json(docs);
  });
  promise.catch((err: Error) => {
    res.status(500).send(err);
  });
});

// Get a Review for a Recipe
router.get('/:recipeId/reviews/:reviewId', (req: Request, res: Response) => {
  const { recipeId, reviewId } = req.params;
  const promise = Review.find({ recipeId, _id: reviewId }).exec();
  promise.then((docs: any) => {
    res.json(docs);
  });
  promise.catch((err: Error) => {
    res.status(500).send(err);
  });
});

// Create a Review for a Recipe
router.post('/:recipeId/reviews', (req: Request, res: Response) => {
  const { recipeId } = req.params;
  const requestReview: IReview = req.body;
  requestReview.recipeId = mongoose.Types.ObjectId(recipeId);

  const reviewToSave = new Review(requestReview);
  const promise = reviewToSave.save();
  promise.then((doc: any) => {
    res.status(201).json(doc);
  });
  promise.catch((err: Error) => {
    res.status(500).send(err);
  });
});

// Update a Review for a Recipe
router.put('/:recipeId/reviews/:reviewId', (req: Request, res: Response) => {
  // TODO: Check if Exists first
  const { recipeId, reviewId } = req.params;
  const requestReview: IReview = req.body;

  const promise = Review.findOneAndUpdate(
    { recipeId, _id: reviewId },
    requestReview,
  ).exec();
  promise.then((doc: any) => {
    res.status(200).json(doc);
  });
  promise.catch((err: Error) => {
    res.status(500).send(err);
  });
});

// Delete a Review for a Recipe
router.delete('/:recipeId/reviews/:reviewId', (req: Request, res: Response) => {
  // TODO: Check if Exists first
  const { recipeId, reviewId } = req.params;
  const requestReview: IReview = req.body;

  const promise = Review.findOneAndDelete(
    { recipeId, _id: reviewId },
    requestReview,
  ).exec();
  promise.then((doc: any) => {
    res.status(200).json(doc);
  });
  promise.catch((err: Error) => {
    res.status(500).send(err);
  });
});

module.exports = router;
