const jwt = require('jsonwebtoken');

const SECRET = process.env.JWT_SECRET!;
const EXPIRES_IN = '365 days';

function AuthenticateToken(token: string): boolean {
  try {
    jwt.verify(token, SECRET);
    return true;
  } catch (error) {
    return false;
  }
}

function GenerateAccessToken(username: string) {
  return jwt.sign({ name: username }, SECRET, { expiresIn: EXPIRES_IN });
}

export { AuthenticateToken, GenerateAccessToken };
