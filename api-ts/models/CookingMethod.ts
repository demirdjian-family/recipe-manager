enum CookingMethod {
  STOVE = 'STOVE',
  OVEN = 'OVEN',
  BARBECUE = 'BARBECUE',
  FRYER = 'FRYER',
  WOK = 'WOK',
  GRIDDLE = 'GRIDDLE',
  NONE = 'NONE',
  INVALID = 'INVALID',
}

function ValidCookingMethods(): CookingMethod[] {
  return [
    CookingMethod.STOVE,
    CookingMethod.OVEN,
    CookingMethod.BARBECUE,
    CookingMethod.FRYER,
    CookingMethod.WOK,
    CookingMethod.GRIDDLE,
    CookingMethod.NONE,
  ];
}

export { CookingMethod, ValidCookingMethods };
