import { Schema, model } from 'mongoose';
import { IIngredient, IngredientSchema } from './Ingredient';

import { Category } from './Category';
import { CookingMethod } from './CookingMethod';

interface IRecipe {
  title: string;

  description: string;

  author: string;

  ingredients: IIngredient[];

  steps: string[];

  category: Category;

  cookingMethod: CookingMethod;

  servings: Number;

  images: string[];
}

const RecipeSchema = new Schema<IRecipe>({
  title: { type: String, required: true },

  description: { type: String, required: true },

  author: { type: String, required: true },

  ingredients: { type: [IngredientSchema], required: true },

  steps: { type: [String], required: true },

  category: { type: 'String', enum: Category, required: true },

  cookingMethod: { type: 'String', enum: CookingMethod, default: CookingMethod.NONE },

  servings: { type: Number, required: true, min: 1 },

  images: { type: [String], required: false, default: [] },
}, { timestamps: true });

RecipeSchema.index({
  title: 'text',
  description: 'text',
  author: 'text',
  'ingredients.name': 'text',
  'ingredients.flatName': 'text',
}, { name: 'recipes-text-index' });

const Recipe = model<IRecipe>('Recipe', RecipeSchema);

export { IRecipe, Recipe, RecipeSchema };
