import { Schema, model } from 'mongoose';
import { Recipe } from './Recipe';

interface IReview {
  recipeId: Schema.Types.ObjectId;

  author: string;

  score: number;

  comment: string;
}

const ReviewSchema = new Schema<IReview>({
  recipeId: { type: Schema.Types.ObjectId, ref: Recipe, required: false },

  author: { type: String, required: true },

  score: {
    type: Number, required: true, min: 1, max: 5,
  },

  comment: { type: String, required: true },
}, { timestamps: true });

const Review = model<IReview>('Review', ReviewSchema);

export { IReview, Review, ReviewSchema };
