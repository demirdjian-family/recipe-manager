import { Schema, model } from 'mongoose';

interface IIngredient {
  name: string;

  quantity: number;

  unit: string;

  flatName: string;

}
const IngredientSchema = new Schema<IIngredient>({
  name: { type: String, required: false },

  quantity: { type: Number, required: false, min: 1 },

  unit: { type: String, required: false },

  flatName: { type: String, required: false },
}, { _id: false, autoCreate: false });

const Ingredient = model<IIngredient>('Ingredient', IngredientSchema);

export { IIngredient, Ingredient, IngredientSchema };
