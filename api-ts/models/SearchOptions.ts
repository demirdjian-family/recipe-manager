import { FilterQuery, SortOrder, QueryOptions } from 'mongoose';
import { Category } from './Category';
import { CookingMethod } from './CookingMethod';
import { IRecipe } from './Recipe';

class SearchOptions {
  private static VALID_SORT_FILEDS = ['createdAt', 'title'];

  queryString?: string;

  category?: Category;

  cookingMethod?: CookingMethod;

  createdBefore?: Date;

  createdAfter?: Date;

  limit?: number;

  order?: SortOrder;

  sortBy?: string;

  public AssembleFilter():FilterQuery<IRecipe> {
    const filter: FilterQuery<IRecipe> = {};
    if (this.queryString) {
      filter.$text = { $search: this.queryString };
    }
    if (this.category) {
      filter.category = this.category;
    }
    if (this.cookingMethod) {
      filter.cookingMethod = this.cookingMethod;
    }

    if (this.createdAfter || this.createdBefore) {
      filter.createdAt = {};
    }
    if (this.createdAfter) {
      filter.createdAt.$gte = this.createdAfter;
    }
    if (this.createdBefore) {
      filter.createdAt.$lte = this.createdBefore;
    }
    return filter;
  }

  public AssembleOptions():QueryOptions {
    const options: QueryOptions = {};
    if (this.limit) {
      options.limit = this.limit;
    }
    if (this.sortBy && SearchOptions.VALID_SORT_FILEDS.includes(this.sortBy)) {
      options.sort = {};
      options.sort[this.sortBy] = this.order ? this.order : -1;
    }

    return options;
  }

  public static from(json: JSON): SearchOptions {
    return Object.assign(new SearchOptions(), json);
  }
}

export default SearchOptions;
