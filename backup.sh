#!/bin/bash
# This script is intended to run on the backup location of your choice.
# It expects an ssh key at $KEY_PATH

KEY_PATH="./keys/id_rsa"
REMOTE_HOST="recipes.demirdjian.family"
REMOTE_USER="deployer"
DB_CONTAINER="recipe-manager_db"
MAX_BACKUPS=7
NOW=`date +"%F"`

# CHECK IF KEY EXISTS
if [ ! -f "$KEY_PATH" ]; then
    echo "$KEY_PATH does not exist."
	exit 1
fi

echo "Backing up Recipe Manager Database."

# BACKUP RECIPES
ssh -i $KEY_PATH $REMOTE_USER@$REMOTE_HOST -T docker exec -t $DB_CONTAINER bash -c '"mongoexport -d "recipe-manager" -c "recipes" -u \$MONGO_INITDB_ROOT_USERNAME -p \$MONGO_INITDB_ROOT_PASSWORD --authenticationDatabase=admin --out recipe-backup.json"'

# BACKUP REVIEWS
ssh -i $KEY_PATH $REMOTE_USER@$REMOTE_HOST -T docker exec -t $DB_CONTAINER bash -c '"mongoexport -d "recipe-manager" -c "reviews" -u \$MONGO_INITDB_ROOT_USERNAME -p \$MONGO_INITDB_ROOT_PASSWORD --authenticationDatabase=admin --out review-backup.json"'

echo "Copying backups out of Docker container."

# COPY BACKUPS TO HOME DIR
ssh -i $KEY_PATH $REMOTE_USER@$REMOTE_HOST -T docker cp $DB_CONTAINER:/recipe-backup.json .
ssh -i $KEY_PATH $REMOTE_USER@$REMOTE_HOST -T docker cp $DB_CONTAINER:/review-backup.json .

echo "Copying backups to respective folders."

# SCP BACKUP FILES HERE
scp -i $KEY_PATH $REMOTE_USER@$REMOTE_HOST:~/recipe-backup.json ../recipes/recipe-backup-$NOW.json
scp -i $KEY_PATH $REMOTE_USER@$REMOTE_HOST:~/review-backup.json ../reviews/review-backup-$NOW.json

# LIMIT FILES TO $MAX_BACKUPS
NUM_RECIPE_FILES=`ls ../recipes | wc -l`
NUM_REVIEW_FILES=`ls ../reviews | wc -l`
MAX_FILES=$((MAX_BACKUPS + 1)) # Add onelsl to accommodate for new file

if [ "$NUM_RECIPE_FILES" -eq "$MAX_FILES" ]; then
	echo "Recipe backups have hit max: $MAX_FILES. Deleting oldest backup."
	OLDEST_RECIPE_FILE=`ls ../recipes -1t | tail -1`
	rm ../recipes/$OLDEST_RECIPE_FILE
fi

if [ "$NUM_REVIEW_FILES" -eq "$MAX_FILES" ]; then
	echo "Reviews backups have hit max: $MAX_FILES. Deleting oldest backup."
	OLDEST_REVIEW_FILE=`ls ../reviews -1t | tail -1`
	rm ../reviews/$OLDEST_REVIEW_FILE
fi

echo "Completed Recipe Manager Database Backup."
