// use recipe-manager
db = db.getSiblingDB('recipe-manager');

// create recipes collection
db.createCollection('recipes');

// create reviews collection
db.createCollection('reviews');

// create gridfs collections
db.createCollection('images.files');
db.createCollection('images.chunks');